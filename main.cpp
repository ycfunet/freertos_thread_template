#include "stdio.h"
#include "pico/stdlib.h"
#include "FreeRTOS.h"
#include "task.h"

#include "ledwriter.h"

int main() {
    BaseType_t ret;

    TaskHandle_t ledwriter_thread = NULL;

    LedWriter *led_writer = new LedWriter();

    ret = xTaskCreate(LedWriter::start, "led_writer", 512, led_writer, tskIDLE_PRIORITY, &ledwriter_thread);
    if(ret != pdPASS)
        printf("Error!\n");

    vTaskStartScheduler();

    while(1)
    {
        configASSERT(0);    /* We should never get here */
    }

}
