#ifndef LEDWRITER_H
#define LEDWRITER_H

#include "pico/stdlib.h"
#include "thread.h"

class LedWriter : public Thread
{
public:
    LedWriter();
    void setSwap();
    void setLed(int light);
    void run();

private:
    uint LED_PIN;
    int LED_LIGHT;
};


#endif // LEDWRITER_H
