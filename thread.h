#ifndef THREAD_H
#define THREAD_H

class Thread
{
public:
    static void start(void * pvParameters) {
        Thread *thread = (Thread *)pvParameters;
        thread->run();
    }
    virtual void run() = 0;
};

#endif // THREAD_H
