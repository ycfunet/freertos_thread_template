#include "ledwriter.h"
#include "pico/stdlib.h"
#include "FreeRTOS.h"
#include "task.h"
#include "stdio.h"

LedWriter::LedWriter()
{
    this->LED_PIN = PICO_DEFAULT_LED_PIN;

    gpio_init(this->LED_PIN);
    gpio_set_dir(this->LED_PIN, GPIO_OUT);
}

void LedWriter::setSwap()
{
    if(this->LED_LIGHT == 0)
        setLed(1);
    else
        setLed(0);
}

void LedWriter::setLed(int light)
{
    this->LED_LIGHT = light;
    gpio_put(this->LED_PIN, this->LED_LIGHT);
}

void LedWriter::run()
{
    setLed(0);

    while(true) {
        vTaskDelay(120);
        setSwap();
    }
}
